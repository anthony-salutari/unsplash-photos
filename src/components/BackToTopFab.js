import React, {Component} from 'react';

import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import PropTypes from 'prop-types';

const style = theme => ({
  fab: {
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2
  }
});

class BackToTopFab extends Component {
  constructor(props) {
    super(props);

    this.onFabClicked = this.onFabClicked.bind(this);
  }

  onFabClicked() {
    window.scrollTo(0, 0);
  }

  render() {
    const {classes} = this.props;
    return (<Button variant='fab' className={classes.fab} color='secondary' onClick={this.onFabClicked}>
      <ExpandLessIcon/>
    </Button>);
  }
}

BackToTopFab.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(style)(BackToTopFab)
