import React, {Component} from 'react';

import SearchBar from 'material-ui-search-bar'

export default class SearchBarComponent extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.submitSearch = this.submitSearch.bind(this);

    this.state = {
      query: ''
    };
  }

  handleChange(value) {
    this.setState({query: value});
  }

  submitSearch() {
    this.props.setInitialState();
    this.props.updateQuery(this.state.query);
  }

  render() {
    return (<SearchBar onChange={(value) => this.handleChange(value)} onRequestSearch={this.submitSearch} style={{
      margin: '0 auto'
        // marginLeft: 'auto',
        // marginRight: 'auto',
        // marginTop: '0',
        // marginBottom: '16px'
      }}/>)
  }
}
