import React, {Component} from 'react';

import {withStyles} from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';

import InfiniteScroll from 'react-infinite-scroll-component';

import Unsplash, {toJson} from 'unsplash-js';

import * as Constants from '../constants';
import PictureCard from './PictureCard';
import SearchBarComponent from './SearchBarComponent';

// import './ImageGrid.css';

const styles = theme => ({
  imageGrid: {
    margin: theme.spacing.unit * 2
  },
  infiniteScroll: {
    overflow: 'none'
  },
  grid: {
    marginTop: theme.spacing.unit
  }
});

const unsplash = new Unsplash({applicationId: Constants.ACCESS_KEY, secret: Constants.SECRET_KEY, callbackUrl: Constants.CALLBACK_URL});

const initialState = {
  images: [],
  query: '',
  page: 1,
  per_page: 24
};

class ImageGrid extends Component {
  constructor(props) {
    super(props);

    this.loadMoreData = this.loadMoreData.bind(this);
    this.updateQuery = this.updateQuery.bind(this);
    this.setInitialState = this.setInitialState.bind(this);

    this.state = initialState;
  }

  componentDidMount() {
    this.updateData();
  }

  updateData() {
    if (this.state.query) {
      unsplash.search.photos(this.state.query, this.state.page, this.state.per_page).then(toJson).then(json => {
        var results = this.state.images.concat(json.results);
        this.setState({images: results});
      });
    } else {
      unsplash.photos.listPhotos(this.state.page, 24, 'popular').then(toJson).then(json => {
        var results = this.state.images.concat(json);
        this.setState({images: results});
      });
    }
  }

  loadMoreData() {
    this.setState(prevState => ({
      page: prevState.page + 1
    }), () => {
      this.updateData();
    });
  }

  updateQuery(query) {
    this.setState({
      query: query
    }, () => {
      this.updateData();
    });
  }

  setInitialState() {
    this.setState(initialState);
  }

  render() {
    const {classes} = this.props;

    return (<div className={classes.imageGrid}>
      <SearchBarComponent updateQuery={this.updateQuery} setInitialState={this.setInitialState}/>
      <InfiniteScroll style={{overflow: 'none'}} dataLength={this.state.images.length} next={this.loadMoreData} hasMore={true} loader={<div style = {{textAlign: 'center', marginTop: '16px'}} > <CircularProgress color='secondary'/>
      </div>}>
        <Grid className={classes.grid} container spacing={16}>
          {
            this.state.images.map((image) => {
              return (<Grid key={image.id} item xs={12} sm={6} lg={4} xl={3}>
                <PictureCard image={image}/>
              </Grid>);
            })
          }
        </Grid>
      </InfiniteScroll>
    </div>);
  }
}

ImageGrid.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(ImageGrid);
