import React from 'react';

import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import PropTypes from 'prop-types';
import Switch from '@material-ui/core/Switch';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  themeSwitch: {
    marginLeft: 'auto',
    backgroundColor: 'inherit'
  }
});

function Header(props) {
  const {classes} = props;

  return (<AppBar position='static' color='primary'>
    <Toolbar>
      <Typography variant='title'>
        Testing
      </Typography>
      <FormControlLabel className={classes.themeSwitch} control={<Switch
        checked = {
          props.darkTheme
        }
        onChange = {
          props.changeTheme
        } />
} label='Dark Theme'/>
    </Toolbar>
  </AppBar>);
}

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Header);
