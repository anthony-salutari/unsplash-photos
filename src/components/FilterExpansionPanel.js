import React, {Component} from 'react';

import {withStyles} from '@material-ui/core/styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import PropTypes from 'prop-types';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120
  },
  textField: {
    margin: theme.spacing.unit,
    width: 200
  }
});

const WAIT_INTERVAL = 1000;
const ENTER_KEY = 13;

class FilterExpansionPanel extends Component {
  constructor(props) {
    super(props);

    this.inputChanged = this.inputChanged.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.triggerChange = this.triggerChange.bind(this);

    this.state = {
      query: ''
    };
  }

  inputChanged(event) {
    this.setState({query: event.target.value});
  }

  handleKeyDown(event) {
    if (event.keyCode === ENTER_KEY) {
      this.triggerChange();
    }
  }

  triggerChange() {
    this.props.setInitialState();
    this.props.updateQuery(this.state.query);
  }

  render() {
    const {classes} = this.props;

    return (<ExpansionPanel>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography>Filter Images</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        <TextField id='query' label='Query' className={classes.textField} onChange={this.inputChanged} onKeyDown={this.handleKeyDown} onBlur={this.triggerChange} margin='normal'/>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor='orientation'>Orientation</InputLabel>
          <Select inputProps={{
              name: 'orientation',
              id: 'orientation'
            }}>
            <MenuItem value=''>
              <em>All</em>
            </MenuItem>
            <MenuItem value={'landscape'}>Landscape</MenuItem>
            <MenuItem value={'portrait'}>Portrait</MenuItem>
            <MenuItem value={'squarish'}>Squarish</MenuItem>
          </Select>
        </FormControl>
      </ExpansionPanelDetails>
    </ExpansionPanel>);
  }
}

FilterExpansionPanel.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(FilterExpansionPanel);
