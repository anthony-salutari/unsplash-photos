import React, {Component} from 'react';

import {withStyles} from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  media: {
    height: 0,
    paddingTop: '75%'
  },
  cardContent: {
    display: 'flex'
  },
  likeText: {
    marginLeft: 'auto',
    marginTop: theme.spacing.unit
  },
  avatar: {
    display: 'inline-flex'
  }
});

class PictureCard extends Component {

  render() {
    const {classes} = this.props;

    return (<Card>
      <CardMedia image={this.props.image.urls.regular} className={classes.media}/>
      <CardContent className={classes.cardContent}>
        <Avatar src={this.props.image.user.profile_image.small} className={classes.avatar}/>
        <Button href={this.props.image.user.links.html}>
          {this.props.image.user.name}
        </Button>
        <Typography className={classes.likeText} variant='body2'>
          {this.props.image.likes} Likes</Typography>
      </CardContent>
      <CardActions>
        <Button size='small' color='secondary' href={this.props.image.links.download}>
          Download
        </Button>
      </CardActions>
    </Card>);
  }
}

PictureCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(PictureCard)
