export const ACCESS_KEY = 'get your own key';

export const SECRET_KEY = 'get your own key';

export const CALLBACK_URL = 'urn:ietf:wg:oauth:2.0:oob';

export const THEME_KEY = 'darkTheme';
