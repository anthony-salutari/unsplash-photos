import {
  createMuiTheme
} from 'material-ui/styles';

export default createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      light: '#8eacbb',
      main: '#607d8b',
      dark: '#34515e'
    },
    secondary: {
      light: '#ffc947',
      main: '#ff9800',
      dark: '#c66900'
    }
  }
});
