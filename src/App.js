import React, {Component} from 'react';

import DarkTheme from './themes/DarkTheme';
import LightTheme from './themes/LightTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import * as Constants from './constants';
import Header from './components/Header';
import ImageGrid from './components/ImageGrid';
import BackToTopFab from './components/BackToTopFab';

const localStorage = window.localStorage;

class App extends Component {
  constructor(props) {
    super(props);

    this.changeTheme = this.changeTheme.bind(this);

    var themeStorage = localStorage.getItem(Constants.THEME_KEY);
    if (themeStorage === null) {
      // localstorage isn't set, make light theme the default
      localStorage.setItem(Constants.THEME_KEY, false);
    }

    console.log(localStorage.getItem(Constants.THEME_KEY));
    this.state = {
      darkTheme: false
    };
  }

  changeTheme(event) {
    localStorage.setItem(Constants.THEME_KEY, event.target.checked);
    this.setState({darkTheme: event.target.checked});
  }

  render() {
    if (this.state.darkTheme) {
      var theme = DarkTheme;
      document.body.style = 'background: #303030';
    } else {
      var theme = LightTheme;
      document.body.style = 'background: #fafafa';
    }

    return (<MuiThemeProvider theme={theme}>
      <div>
        <Header darkTheme={this.state.darkTheme} changeTheme={this.changeTheme}/>
        <ImageGrid/>
        <BackToTopFab/>
      </div>
    </MuiThemeProvider>);
  }
}

export default App;
